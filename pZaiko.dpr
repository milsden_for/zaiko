program pZaiko;

uses
  Vcl.Forms,
  uMain in 'uMain.pas' {fMain},
  uInput in 'uInput.pas' {fInput},
  uSale in 'uSale.pas' {fSale},
  uClientControl in 'uClientControl.pas' {fClientControl},
  uInventoryControl in 'uInventoryControl.pas' {fInventoryControl},
  uTotals in 'uTotals.pas' {fTotals},
  udmMongo in 'udmMongo.pas' {dmMongo: TDataModule},
  uDefaultRegistry in 'uDefaultRegistry.pas' {fDefaultRegistry},
  uClientRegistry in 'uClientRegistry.pas' {fClientRegistry},
  uProductRegistry in 'uProductRegistry.pas' {fProductRegistry},
  ufrmProduct in 'ufrmProduct.pas' {frmProduct: TFrame},
  uFunctionsDB in 'uFunctionsDB.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfMain, fMain);
  Application.CreateForm(TdmMongo, dmMongo);
  Application.Run;
end.
