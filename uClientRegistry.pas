unit uClientRegistry;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uDefaultRegistry, Vcl.Mask, System.UITypes,
  Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls, Data.DB, Vcl.DBCtrls,
  FireDAC.Phys.MongoDBWrapper, System.JSON.Types;

type
  TfClientRegistry = class(TfDefaultRegistry)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    edtName: TEdit;
    metCPF: TMaskEdit;
    metMobile: TMaskEdit;
    edtEmail: TEdit;
    procedure sbnExitClick(Sender: TObject);
    procedure sbnCancelClick(Sender: TObject);
    procedure sbnSaveClick(Sender: TObject);
    procedure sbnDeleteClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure InsertData;
    procedure UpdateData;
  public
    { Public declarations }
  end;

var
  fClientRegistry: TfClientRegistry;

implementation

{$R *.dfm}

uses udmMongo, uClientControl;

procedure TfClientRegistry.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;

  fClientRegistry := nil;
end;

procedure TfClientRegistry.FormShow(Sender: TObject);
begin
  inherited;

  if Assigned(fClientControl) then
  begin
    edtId.Text := fClientControl.grdClient.DataSource.DataSet.Fields[0].AsString;
    edtName.Text := fClientControl.grdClient.DataSource.DataSet.Fields[1].AsString;
    metCPF.Text := fClientControl.grdClient.DataSource.DataSet.Fields[2].AsString;
    metMobile.Text := fClientControl.grdClient.DataSource.DataSet.Fields[3].AsString;
    edtEmail.Text := fClientControl.grdClient.DataSource.DataSet.Fields[4].AsString;
  end;
end;

procedure TfClientRegistry.InsertData;
begin
  try
    dmMongo.FConMongo['zaiko']['client'].Insert().Values()
      .Add('full_name', edtName.Text)
      .Add('cpf', metCPF.Text)
      .Add('mobile', metMobile.Text)
      .Add('email', edtEmail.Text).&End.Exec;
    sbnCancel.Click;
    MessageDlg('Salvo com sucesso!', mtInformation, [mbOK], 0);
  except
    on E: Exception do
      MessageDlg('Erro ao salvar: ' + E.Message, mtWarning, [mbOK], 0);
  end;
end;

procedure TfClientRegistry.sbnCancelClick(Sender: TObject);
begin
  edtName.Clear;
  metCPF.Text := '   .   .   -  ';
  metMobile.Text := '(  )     -    ';
  edtEmail.Clear;
  edtName.SetFocus;
end;

procedure TfClientRegistry.sbnDeleteClick(Sender: TObject);
begin
  inherited;

  try
    dmMongo.FConMongo['zaiko']['client'].Remove().Match()
      .Add('_id', edtId.Text).&End.Exec;
    sbnCancel.Click;
    MessageDlg('Exclu�do com sucesso!', mtInformation, [mbOK], 0);
  except
    on E: Exception do
      MessageDlg('Erro ao excluir: ' + E.Message, mtWarning, [mbOK], 0);
  end;
end;

procedure TfClientRegistry.sbnExitClick(Sender: TObject);
begin
  inherited;

  Close;
end;

procedure TfClientRegistry.sbnSaveClick(Sender: TObject);
begin
  inherited;

  if edtId.Text = '' then
    InsertData
  else
    UpdateData
end;

procedure TfClientRegistry.UpdateData;
var
  lv_id: TJsonOid;
begin
  lv_id := TJsonOid.Create(edtId.Text);

  try
    dmMongo.FConMongo['zaiko']['client'].Update().Match()
      .Add('_id', lv_id).&End
      .Modify().&Set()
        .Field('full_name', edtName.Text)
        .Field('cpf', metCPF.Text)
        .Field('mobile', metMobile.Text)
        .Field('email', edtEmail.Text).&End
      .&End.Exec;
    sbnCancel.Click;
    MessageDlg('Salvo com sucesso!', mtInformation, [mbOK], 0);
  except
    on E: Exception do
      MessageDlg('Erro ao salvar: ' + E.Message, mtWarning, [mbOK], 0);
  end;
end;

end.
