object frmProduct: TfrmProduct
  Left = 0
  Top = 0
  Width = 505
  Height = 36
  TabOrder = 0
  object Label4: TLabel
    Left = 7
    Top = 6
    Width = 54
    Height = 19
    Caption = 'Produto:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Sylfaen'
    Font.Style = []
    ParentFont = False
  end
  object edtCode: TEdit
    Left = 67
    Top = 3
    Width = 87
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Sylfaen'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnExit = edtCodeExit
  end
  object edtName: TEdit
    Left = 160
    Top = 3
    Width = 338
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Sylfaen'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
end
