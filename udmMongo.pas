unit udmMongo;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MongoDB,
  FireDAC.Phys.MongoDBDef, System.Rtti, System.JSON.Types, System.JSON.Readers,
  System.JSON.BSON, System.JSON.Builders, FireDAC.Phys.MongoDBWrapper,
  FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client, FireDAC.Comp.UI,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.Comp.DataSet,
  FireDAC.Phys.MongoDBDataSet, FireDAC.Stan.StorageBin;

type
  TdmMongo = class(TDataModule)
    FDConnection1: TFDConnection;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDPhysMongoDriverLink1: TFDPhysMongoDriverLink;
    mtbSalesItems: TFDMemTable;
    dseSalesItems: TDataSource;
    mqyProduct: TFDMongoQuery;
    dseProduct: TDataSource;
    mqyClient: TFDMongoQuery;
    dseClient: TDataSource;
    dseEspenses: TDataSource;
    dseTotalDaily: TDataSource;
    mtbTotalDaily: TFDMemTable;
    mqyExpenses: TFDMongoQuery;
    mtbTotalDailymtbTotalDailyFieldSaleDate: TDateField;
    mtbTotalDailymtbTotalDailyFieldSubtotal: TFloatField;
    mtbTotalDailymtbTotalDailyFieldWayPay: TStringField;
    mqyHistoricClient: TFDMongoQuery;
    dseHistoricClient: TDataSource;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FConMongo: TMongoConnection;
    FEnv: TMongoEnv;
  end;

var
  dmMongo: TdmMongo;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdmMongo.DataModuleCreate(Sender: TObject);
begin
  FDConnection1.Connected := True;
  FConMongo := TMongoConnection(FDConnection1.CliObj);
  FEnv := FConMongo.Env;
  mqyProduct.DatabaseName := 'zaiko';
  mqyProduct.CollectionName := 'product';
  mqyClient.DatabaseName := 'zaiko';
  mqyClient.CollectionName := 'client';
  mqyExpenses.DatabaseName := 'zaiko';
  mqyExpenses.CollectionName := 'expenses';
  mqyHistoricClient.DatabaseName := 'zaiko';
  mqyHistoricClient.CollectionName := 'sales';
end;

procedure TdmMongo.DataModuleDestroy(Sender: TObject);
begin
  FDConnection1.Connected := False;
end;

end.
