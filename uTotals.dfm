object fTotals: TfTotals
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsSingle
  ClientHeight = 508
  ClientWidth = 778
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlButtons: TPanel
    Left = 0
    Top = 451
    Width = 778
    Height = 57
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      778
      57)
    object sbnExit: TSpeedButton
      Left = 673
      Top = 6
      Width = 97
      Height = 44
      Cursor = crHandPoint
      Hint = 'Sair'
      Anchors = [akRight, akBottom]
      Flat = True
      Glyph.Data = {
        36080000424D3608000000000000360400002800000020000000200000000100
        0800000000000004000027080000270800000001000000010000BDB9B2009A94
        8C0080796F00ECEBE900FFFFFF00EDECE700DFDED800AEAAA2007C7469005349
        3C004F453800E3E2E000E4E2DD00B5B1A900827B7100564C4000E7E6E100BCB8
        B100898379005A504400EBE9E400C3BFB80051483B00544A3D00E3ECFB0071A1
        E900F4F8FD00F3F7FD004E89E400528CE400F2F6FD00678FCC006A9CE8006699
        E7002C72DE002A71DE007E9FD00087B0EC007AA7EB003D7EE000387BE0006C9D
        E800FCFDFF00EBEAE500DFDED70096978500B1B2A400D5D5CC006B9DE800DBDA
        D300B7B7AA0098998800E8E8E200E3E2DB00699CE700FBFDFE00EEF4FC00689B
        E800ECF2FB0093B7ED00D6D4CE009F9992009C968F00F0EFEE00FEFEFD00F8F8
        F600F2F2EE00EDECE800FEFEFE00F8F8F700F4F2EF00EEEDE900FAFAF800F3F3
        F000FAFAFA00F5F5F1000000000066000000000000000000A400000000002F00
        00000001000000000000A4000000000000000000DB00000000002F0000000001
        000000000000DB0000000000000000001C00010000002F000000000100000000
        00001C0100000000000000005400010000002F00000000010000000000005401
        00000000000000005900010000002F0000000001000000000000590100000000
        000000008A00010000002F00000000010000000000008A010000000000000000
        BE00010000002F0000000001000000000000BE010000000000000000EF000100
        00002F0000000001000000000000EF0100000000000000002000020000002F00
        00000001000000000000200200000000000000005A00020000002F0000000001
        0000000000005A0200000000000000009100020000002F000000000100000000
        000091020000000000000000C500020000002F0000000001000000000000C502
        0000000000000000CA00020000002F0000000001000000000000CA0200000000
        00000000FB00020000002F0000000001000000000000FB020000000000000000
        2C00030000002F00000000010000000000002C0300000000000000005D000300
        00002F00000000010000000000005D0300000000000000008E00030000002F00
        000000010000000000008E0300000000000000009300030000002F0000000001
        00000000000093030000000000000000C400030000002F000000000100000000
        0000C4030000000000000000F500030000002F0000000001000000000000F503
        00000000000000002900040000002F0000000001000000000000290400000000
        000000002E00040000002F00000000010000000000002E040000000000000000
        5F00040000002F00000000010000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000040404040404
        04040404044A4B04040404040404040404040404040404040404040404040404
        0404484947050544040404040404040404040404040404040404040404044445
        4647050505050544040404040404040404040404040404040404044041424305
        05050505050505440404040404040404040404040404040404043C2B05050505
        050505050505053D3E3E3E3E3E3E3F0404040404040404040404050505050505
        05050505050505170A0A0A0A0A0A0B0404040404040404040404050505050505
        05050505050505170A0A0A0A0A0A0B0404040404040404040404050505050505
        05050505050505170A0A0A0A0A0A0B0404040404040404040404050505050505
        05050505050505170A0A0A0A0A0A0B0404040404040404040404050505050505
        05050505050505170A0A0A0A0A0A0B0404040404040404040404050505050505
        05050505050505170A0A0A0A0A0A0B04043A3B37040404040404050505050505
        05050505050505170A0A0A0A0A0A0B0404382839370404040404050505050505
        05050505343505170A0A0A0A0A0A0B0404041828363704040404050505050505
        05053132332E05170A0A0A0A0A0A0B040404041828202A040404050505050505
        052C2D2E2F0505170A0A0A0A0A0A0B04040404041828302A0404050505050505
        05052B05050505170A0A0A0A0A0A0B0404040404041828292A04050505050505
        05050505050505170A0A0A0A0A0A0B040404040404041828292A050505050505
        05050505050505170A0A0A0A0A0A242525252525252525262327050505050505
        05050505050505170A0A0A0A0A0A1F2020202020202020212223050505050505
        05050505050505170A0A0A0A0A0A0B040404040404041E1C1D1A050505050505
        05050505050505170A0A0A0A0A0A0B0404040404041E1C1D1A04050505050505
        05050505050505170A0A0A0A0A0A0B04040404041E1C1D1A0404050505050505
        05050505050505170A0A0A0A0A0A0B040404041E1C1D1A040404050505050505
        05050505050505170A0A0A0A0A0A0B0404041E1C1D1A04040404050505050505
        05050505050505170A0A0A0A0A0A0B04041B1C1D1A0404040404050505050505
        05050505050505170A0A0A0A0A0A0B040418191A040404040404050505050505
        05050505050505170A0A0A0A0A0A0B0404040404040404040404050505050505
        05050505051415160A0A0A0A0A0A0B0404040404040404040404050505050505
        0505101112130A0A0A0A0A0A0A0A0B040404040404040404040405050505050C
        0D0E0F0A0A0A0A0A0A0A0A0A0A0A0B0404040404040404040404050506070809
        0A0A0A0A0A0A0A0A0A0A0A0A0A0A0B0404040404040404040404000102020202
        0202020202020202020202020202030404040404040404040404}
      ParentShowHint = False
      ShowHint = True
      OnClick = sbnExitClick
      ExplicitLeft = 842
    end
    object sttExpensiveTotal: TStaticText
      Left = 4
      Top = 6
      Width = 140
      Height = 26
      Caption = 'Total de Gastos: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'Sylfaen'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      Visible = False
    end
    object sttTotalDaily: TStaticText
      Left = 4
      Top = 6
      Width = 115
      Height = 26
      Caption = 'Total Di'#225'rio: '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'Sylfaen'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      Visible = False
    end
  end
  object pclSubtotals: TPageControl
    Left = 0
    Top = 46
    Width = 778
    Height = 405
    ActivePage = tstSubTotals
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Sylfaen'
    Font.Style = []
    ParentFont = False
    Style = tsFlatButtons
    TabOrder = 1
    OnChange = pclSubtotalsChange
    object tstSubTotals: TTabSheet
      Caption = 'Subtotais'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Sylfaen'
      Font.Style = []
      ParentFont = False
      object pnlSubtotals: TPanel
        Left = 0
        Top = 0
        Width = 770
        Height = 368
        Align = alClient
        BevelKind = bkTile
        BevelOuter = bvNone
        Color = clWindow
        ParentBackground = False
        TabOrder = 0
        object Label9: TLabel
          Left = 246
          Top = 53
          Width = 54
          Height = 19
          Caption = 'A prazo:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Sylfaen'
          Font.Style = []
          ParentFont = False
        end
        object Label8: TLabel
          Left = 26
          Top = 152
          Width = 50
          Height = 19
          Caption = 'Cheque:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Sylfaen'
          Font.Style = []
          ParentFont = False
        end
        object Label2: TLabel
          Left = 21
          Top = 53
          Width = 55
          Height = 19
          Caption = 'Dinheiro:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Sylfaen'
          Font.Style = []
          ParentFont = False
        end
        object Label3: TLabel
          Left = 26
          Top = 86
          Width = 50
          Height = 19
          Caption = 'Cr'#233'dito:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Sylfaen'
          Font.Style = []
          ParentFont = False
        end
        object Label4: TLabel
          Left = 213
          Top = 119
          Width = 87
          Height = 19
          Caption = 'Pagamento D:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Sylfaen'
          Font.Style = []
          ParentFont = False
        end
        object Label6: TLabel
          Left = 214
          Top = 86
          Width = 86
          Height = 19
          Caption = 'Pagamento C:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Sylfaen'
          Font.Style = []
          ParentFont = False
        end
        object Label7: TLabel
          Left = 30
          Top = 119
          Width = 46
          Height = 19
          Caption = 'D'#233'bito:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Sylfaen'
          Font.Style = []
          ParentFont = False
        end
        object edtDebt: TEdit
          Left = 82
          Top = 116
          Width = 87
          Height = 27
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Sylfaen'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          OnChange = edtCashChange
        end
        object edtCredit: TEdit
          Left = 82
          Top = 83
          Width = 87
          Height = 27
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Sylfaen'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          OnChange = edtCashChange
        end
        object edtCheck: TEdit
          Left = 82
          Top = 149
          Width = 87
          Height = 27
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Sylfaen'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          OnChange = edtCashChange
        end
        object edtPayD: TEdit
          Left = 306
          Top = 116
          Width = 87
          Height = 27
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Sylfaen'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
          OnChange = edtCashChange
        end
        object edtPayC: TEdit
          Left = 306
          Top = 83
          Width = 87
          Height = 27
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Sylfaen'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 4
          OnChange = edtCashChange
        end
        object edtForward: TEdit
          Left = 306
          Top = 50
          Width = 87
          Height = 27
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Sylfaen'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 5
          OnChange = edtCashChange
        end
        object edtCash: TEdit
          Left = 82
          Top = 50
          Width = 87
          Height = 27
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Sylfaen'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 6
          OnChange = edtCashChange
        end
      end
    end
    object tstExpenses: TTabSheet
      Caption = 'Gastos'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Sylfaen'
      Font.Style = []
      ImageIndex = 1
      ParentFont = False
      object grdExpenses: TDBGrid
        Left = 0
        Top = 0
        Width = 770
        Height = 368
        Align = alClient
        DataSource = dmMongo.dseEspenses
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -15
        TitleFont.Name = 'Sylfaen'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = '_id'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'month'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'year'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'historic'
            Title.Caption = 'Descri'#231#227'o'
            Width = 465
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'type'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'value'
            Title.Caption = 'Valor'
            Width = 117
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'skill'
            Title.Caption = 'Compet'#234'ncia'
            Width = 89
            Visible = True
          end>
      end
    end
    object tstTodalDaily: TTabSheet
      Caption = 'Total Di'#225'rio'
      ImageIndex = 2
      object grdTotalDaily: TDBGrid
        Left = 0
        Top = 0
        Width = 770
        Height = 368
        Align = alClient
        DataSource = dmMongo.dseTotalDaily
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -15
        TitleFont.Name = 'Sylfaen'
        TitleFont.Style = []
        OnCellClick = grdTotalDailyCellClick
        Columns = <
          item
            Expanded = False
            FieldName = 'mtbTotalDailyFieldSaleDate'
            Title.Caption = 'Venda'
            Width = 114
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'mtbTotalDailyFieldSubtotal'
            Title.Caption = 'Total'
            Width = 122
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'mtbTotalDailyFieldWayPay'
            Title.Caption = 'Forma de Pagamento'
            Width = 180
            Visible = True
          end>
      end
    end
  end
  object pnlTitle: TPanel
    Left = 0
    Top = 0
    Width = 778
    Height = 46
    Align = alTop
    BevelOuter = bvSpace
    Caption = 'Total Mensal'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Sylfaen'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    DesignSize = (
      778
      46)
    object Label1: TLabel
      Left = 564
      Top = 14
      Width = 52
      Height = 19
      Anchors = [akTop, akRight]
      Caption = 'Per'#237'odo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Sylfaen'
      Font.Style = []
      ParentFont = False
      ExplicitLeft = 733
    end
    object dprPeriod: TDatePicker
      Left = 620
      Top = 10
      Height = 27
      Anchors = [akTop, akRight]
      Date = 43675.000000000000000000
      DateFormat = 'MM/yyyy'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Sylfaen'
      Font.Style = []
      TabOrder = 0
      OnChange = dprPeriodChange
    end
  end
end
