unit uDefaultRegistry;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, Vcl.ExtCtrls, Vcl.StdCtrls,
  System.ImageList, Vcl.ImgList, System.Actions, Vcl.ActnList;

type
  TfDefaultRegistry = class(TForm)
    Label5: TLabel;
    Bevel1: TBevel;
    sbnSave: TSpeedButton;
    sbnCancel: TSpeedButton;
    sbnExit: TSpeedButton;
    sbnDelete: TSpeedButton;
    edtId: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fDefaultRegistry: TfDefaultRegistry;

implementation

{$R *.dfm}

procedure TfDefaultRegistry.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  fDefaultRegistry := nil;
end;

end.
