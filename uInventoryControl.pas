unit uInventoryControl;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, Data.DB, Vcl.ExtCtrls,
  Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls, udmMongo, uProductRegistry, uMain, StrUtils;

type
  TfInventoryControl = class(TForm)
    sbnExit: TSpeedButton;
    Label5: TLabel;
    Bevel1: TBevel;
    grdProduct: TDBGrid;
    Panel1: TPanel;
    Label4: TLabel;
    edtCode: TEdit;
    edtName: TEdit;
    sbnSearch: TSpeedButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sbnExitClick(Sender: TObject);
    procedure sbnSearchClick(Sender: TObject);
    procedure edtCodeKeyPress(Sender: TObject; var Key: Char);
    procedure grdProductDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fInventoryControl: TfInventoryControl;

implementation

{$R *.dfm}

procedure TfInventoryControl.edtCodeKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    sbnSearch.OnClick(Self);
end;

procedure TfInventoryControl.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  fInventoryControl := nil;
end;

procedure TfInventoryControl.grdProductDblClick(Sender: TObject);
begin
  fMain.OpenForm(TfProductRegistry, fProductRegistry);
end;

procedure TfInventoryControl.sbnExitClick(Sender: TObject);
begin
  dmMongo.mqyProduct.Close;
  Close;
end;

procedure TfInventoryControl.sbnSearchClick(Sender: TObject);
var
  lv_match: string;
begin
  if edtCode.Text <> '' then
    lv_match := '{code:' + QuotedStr(edtCode.Text) + '}';

  if edtName.Text <> '' then
  begin
    if lv_match <> '' then
      lv_match := lv_match.Substring(0, lv_match.Length - 1) + ', {'
    else
      lv_match := '{';

    lv_match := lv_match + 'full_name: {''$regex'':' + QuotedStr(edtName.Text) + ',''$options'':''i''}}';
  end;
  dmMongo.mqyProduct.Close;
  dmMongo.mqyProduct.QMatch := IfThen(lv_match <> '', lv_match, '');
  dmMongo.mqyProduct.Open;

  grdProduct.Columns[0].Visible := False;
  grdProduct.Columns[1].Title.Caption := 'C�digo';
  grdProduct.Columns[2].Title.Caption := 'Descri��o';
  grdProduct.Columns[2].Width := 300;
  grdProduct.Columns[3].Title.Caption := 'Tamanho';
  grdProduct.Columns[4].Visible := False;
  grdProduct.Columns[5].Title.Caption := 'Valor';
  grdProduct.Columns[6].Title.Caption := 'Qtde.';
  TNumericField(dmMongo.mqyProduct.Fields[5]).DisplayFormat := '#,##0.00';
end;

end.
