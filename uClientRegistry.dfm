inherited fClientRegistry: TfClientRegistry
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Label5: TLabel
    Left = 208
    Width = 218
    Caption = 'Cadastro de Cliente'
    ExplicitLeft = 208
    ExplicitWidth = 218
  end
  inherited sbnSave: TSpeedButton
    Anchors = [akRight, akBottom]
    OnClick = sbnSaveClick
  end
  inherited sbnCancel: TSpeedButton
    Anchors = [akRight, akBottom]
    OnClick = sbnCancelClick
  end
  inherited sbnExit: TSpeedButton
    Anchors = [akRight, akBottom]
    OnClick = sbnExitClick
  end
  object Label1: TLabel [5]
    Left = 8
    Top = 65
    Width = 105
    Height = 19
    Caption = 'Nome Completo:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Sylfaen'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel [6]
    Left = 82
    Top = 98
    Width = 31
    Height = 19
    Caption = 'CPF:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Sylfaen'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel [7]
    Left = 13
    Top = 131
    Width = 100
    Height = 19
    Caption = 'Telefone M'#243'vel:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Sylfaen'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel [8]
    Left = 70
    Top = 164
    Width = 43
    Height = 19
    Caption = 'E-mail:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Sylfaen'
    Font.Style = []
    ParentFont = False
  end
  inherited sbnDelete: TSpeedButton
    Anchors = [akRight, akBottom]
    OnClick = sbnDeleteClick
  end
  object edtName: TEdit [10]
    Left = 119
    Top = 62
    Width = 507
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Sylfaen'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object metCPF: TMaskEdit [11]
    Left = 119
    Top = 95
    Width = 116
    Height = 27
    EditMask = '999.999.999\-99;1;_'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Sylfaen'
    Font.Style = []
    MaxLength = 14
    ParentFont = False
    TabOrder = 1
    Text = '   .   .   -  '
  end
  object metMobile: TMaskEdit [12]
    Left = 119
    Top = 128
    Width = 119
    Height = 27
    EditMask = '!\(00\)00000-0000;1;_'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Sylfaen'
    Font.Style = []
    MaxLength = 14
    ParentFont = False
    TabOrder = 2
    Text = '(  )     -    '
  end
  object edtEmail: TEdit [13]
    Left = 119
    Top = 161
    Width = 507
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Sylfaen'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
  end
  inherited edtId: TEdit
    TabOrder = 4
  end
end
