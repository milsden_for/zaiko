unit ufrmProduct;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  FireDAC.Phys.MongoDBWrapper, System.JSON, uFunctionsDB;

type
  TfrmProduct = class(TFrame)
    edtCode: TEdit;
    edtName: TEdit;
    Label4: TLabel;
    procedure edtCodeExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    gv_value: Double;
  end;

implementation

{$R *.dfm}

uses udmMongo;

procedure TfrmProduct.edtCodeExit(Sender: TObject);
var
  jName, jValue: TJSONValue;
  jRetorno: TJSONObject;
begin
  if StrToIntDef(edtCode.Text, 0) > 0 then
  begin
    jRetorno := GetSelect(edtCode.Text, 'product', 'code');

    if Assigned(jRetorno) then
    begin
      jName := jRetorno.Get('full_name').JsonValue;
      jValue := jRetorno.Get('value').JsonValue;
      edtName.Text := jName.Value;
      gv_value := StrToFloat(jValue.Value);
    end
    else
      edtCode.Clear;
  end;
end;

end.
