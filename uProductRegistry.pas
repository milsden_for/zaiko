unit uProductRegistry;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uDefaultRegistry, Vcl.Buttons, System.UITypes,
  Vcl.ExtCtrls, Vcl.StdCtrls, VCLTee.TeCanvas, FireDAC.Phys.MongoDBWrapper, System.JSON.Types;

type
  TfProductRegistry = class(TfDefaultRegistry)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    edtCode: TEdit;
    edtName: TEdit;
    edtSize: TEdit;
    cftGenre: TComboFlat;
    edtValue: TEdit;
    procedure edtValueExit(Sender: TObject);
    procedure edtValueKeyPress(Sender: TObject; var Key: Char);
    procedure sbnExitClick(Sender: TObject);
    procedure sbnSaveClick(Sender: TObject);
    procedure sbnCancelClick(Sender: TObject);
    procedure sbnDeleteClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure InsertData;
    procedure UpdateData;
  public
    { Public declarations }
  end;

var
  fProductRegistry: TfProductRegistry;

implementation

{$R *.dfm}

uses udmMongo, uInventoryControl;

procedure TfProductRegistry.edtValueExit(Sender: TObject);
begin
  inherited;

  edtValue.Text := FormatFloat('0.00', StrToFloatDef(edtValue.Text, 0));
end;

procedure TfProductRegistry.edtValueKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  if ((CharInSet(Key, ['0'..'9',',','.']) = False) and (word(Key) <> VK_BACK)) then
    Key := #0;
end;

procedure TfProductRegistry.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;

  fProductRegistry := nil;
end;

procedure TfProductRegistry.FormShow(Sender: TObject);
begin
  inherited;

  if Assigned(fInventoryControl) then
  begin
    edtId.Text := fInventoryControl.grdProduct.DataSource.DataSet.Fields[0].AsString;
    edtCode.Text := fInventoryControl.grdProduct.DataSource.DataSet.Fields[1].AsString;
    edtName.Text := fInventoryControl.grdProduct.DataSource.DataSet.Fields[2].AsString;
    edtSize.Text := fInventoryControl.grdProduct.DataSource.DataSet.Fields[3].AsString;
    cftGenre.ItemIndex := fInventoryControl.grdProduct.DataSource.DataSet.Fields[4].AsInteger;
    edtValue.Text := fInventoryControl.grdProduct.DataSource.DataSet.Fields[5].AsString;
    edtValue.OnExit(Self);
  end;
end;

procedure TfProductRegistry.InsertData;
begin
  try
    dmMongo.FConMongo['zaiko']['product'].Insert().Values()
      .Add('code', edtCode.Text)
      .Add('full_name', edtName.Text)
      .Add('size', edtSize.Text)
      .Add('genre', cftGenre.ItemIndex)
      .Add('value', StrToFloat(edtValue.Text)).&End.Exec;
    sbnCancel.Click;
    MessageDlg('Salvo com sucesso!', mtInformation, [mbOK], 0);
  except
    on E: Exception do
    MessageDlg('Erro ao salvar: ' + E.Message, mtWarning, [mbOK], 0);
  end;
end;

procedure TfProductRegistry.sbnCancelClick(Sender: TObject);
begin
  inherited;

  edtCode.Clear;
  edtName.Clear;
  edtSize.Clear;
  cftGenre.ItemIndex := -1;
  cftGenre.Text := '';
  edtValue.Clear;
  edtCode.SetFocus;
end;

procedure TfProductRegistry.sbnDeleteClick(Sender: TObject);
begin
  inherited;

  try
    dmMongo.FConMongo['zaiko']['product'].Remove().Match()
      .Add('_id', edtId.Text).&End.Exec;
    sbnCancel.Click;
    MessageDlg('Exclu�do com sucesso!', mtInformation, [mbOK], 0);
  except
    on E: Exception do
      MessageDlg('Erro ao excluir: ' + E.Message, mtWarning, [mbOK], 0);
  end;
end;

procedure TfProductRegistry.sbnExitClick(Sender: TObject);
begin
  inherited;

  Close;
end;

procedure TfProductRegistry.sbnSaveClick(Sender: TObject);
begin
  inherited;

  if edtId.Text = '' then
    InsertData
  else
    UpdateData;
end;

procedure TfProductRegistry.UpdateData;
var
  lv_id: TJsonOid;
begin
  lv_id := TJsonOid.Create(edtId.Text);

  try
    dmMongo.FConMongo['zaiko']['product'].Update().Match()
      .Add('_id', lv_id).&End
      .Modify().&Set()
        .Field('code', edtCode.Text)
        .Field('full_name', edtName.Text)
        .Field('size', edtSize.Text)
        .Field('genre', cftGenre.ItemIndex)
        .Field('value', StrToFloat(edtValue.Text)).&End
      .&End.Exec;
    sbnCancel.Click;
      MessageDlg('Salvo com sucesso!', mtInformation, [mbOK], 0);
  except
    on E: Exception do
      MessageDlg('Erro ao salvar: ' + E.Message, mtWarning, [mbOK], 0);
  end;
end;

end.
