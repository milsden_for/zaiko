unit uClientControl;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Grids, Vcl.DBGrids, Vcl.Buttons, Vcl.Mask, uClientRegistry, uMain, System.UITypes,
  udmMongo, StrUtils, Vcl.Menus, FireDAC.Phys.MongoDBWrapper, System.JSON, uFunctionsDB,
  VCLTee.TeCanvas, System.DateUtils, Vcl.ComCtrls;

type
  TfClientControl = class(TForm)
    Label5: TLabel;
    Bevel1: TBevel;
    Label4: TLabel;
    sbnSearch: TSpeedButton;
    grdClient: TDBGrid;
    pnlButtons: TPanel;
    sbnExit: TSpeedButton;
    edtName: TEdit;
    metCPF: TMaskEdit;
    pmuBalanceDue: TPopupMenu;
    mimWriteOff: TMenuItem;
    pnlWriteOff: TPanel;
    Label1: TLabel;
    edtWriteOff: TEdit;
    sbnWriteOff: TSpeedButton;
    Label7: TLabel;
    cftType: TComboFlat;
    sbnClosePanel: TSpeedButton;
    pclBalanceDue: TPageControl;
    tstListClient: TTabSheet;
    tsthistoric: TTabSheet;
    grdHistoric: TDBGrid;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sbnSearchClick(Sender: TObject);
    procedure metCPFKeyPress(Sender: TObject; var Key: Char);
    procedure sbnExitClick(Sender: TObject);
    procedure grdClientDblClick(Sender: TObject);
    procedure mimWriteOffClick(Sender: TObject);
    procedure sbnWriteOffClick(Sender: TObject);
    procedure sbnClosePanelClick(Sender: TObject);
    procedure grdClientKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    function GetForward(aCPF: string): Double;
  public
    { Public declarations }
  end;

var
  fClientControl: TfClientControl;

implementation

{$R *.dfm}

procedure TfClientControl.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  fClientControl := nil;
end;

procedure TfClientControl.grdClientDblClick(Sender: TObject);
begin
  fMain.OpenForm(TfClientRegistry, fClientRegistry);
end;

procedure TfClientControl.grdClientKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
  begin
    dmMongo.mqyHistoricClient.Close;
    dmMongo.mqyHistoricClient.QMatch := '{client:' + QuotedStr(dmMongo.mqyClient.Fields[2].AsString) + '}';
    dmMongo.mqyHistoricClient.Open;

    if not dmMongo.mqyHistoricClient.IsEmpty then
    begin
      grdHistoric.Columns[0].Visible := False;
      grdHistoric.Columns[1].Visible := False;
      grdHistoric.Columns[2].Visible := False;
      grdHistoric.Columns[3].Visible := False;
      grdHistoric.Columns[4].Title.Caption := 'Data';
      grdHistoric.Columns[5].Title.Caption := 'Produto';
      grdHistoric.Columns[5].Width := 200;
      grdHistoric.Columns[6].Title.Caption := 'Valor';
      grdHistoric.Columns[7].Visible := False;
      TNumericField(dmMongo.mqyHistoricClient.Fields[6]).DisplayFormat := '#,##0.00';
      pclBalanceDue.ActivePageIndex := 1;
    end;
  end;
end;

procedure TfClientControl.metCPFKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    sbnSearch.OnClick(Self);
end;

procedure TfClientControl.mimWriteOffClick(Sender: TObject);
begin
  pnlWriteOff.Visible := True;
  edtWriteOff.SetFocus;
end;

procedure TfClientControl.sbnClosePanelClick(Sender: TObject);
begin
  pnlWriteOff.Visible := False;
  edtWriteOff.Clear;
  cftType.ItemIndex := -1;
  cftType.Text := '';
end;

procedure TfClientControl.sbnExitClick(Sender: TObject);
begin
  dmMongo.mqyClient.Close;
  Close;
end;

procedure TfClientControl.sbnSearchClick(Sender: TObject);
var
  lv_match: string;
begin
  if metCPF.Text <> '   .   .   -  ' then
    lv_match := '{cpf:' + QuotedStr(metCPF.Text) + '}';

  if edtName.Text <> '' then
  begin
    if lv_match <> '' then
      lv_match := lv_match.Substring(0, lv_match.Length - 1) + ', {'
    else
      lv_match := '{';

    lv_match := lv_match + 'full_name: {''$regex'':' + QuotedStr(edtName.Text) + ',''$options'':''i''}}';
  end;
  dmMongo.mqyClient.Close;
  dmMongo.mqyClient.QMatch := IfThen(lv_match <> '', lv_match, '');
  dmMongo.mqyClient.Open;

  grdClient.Columns[0].Visible := False;
  grdClient.Columns[1].Title.Caption := 'Nome';
  grdClient.Columns[1].Width := 300;
  grdClient.Columns[2].Title.Caption := 'CPF';
  grdClient.Columns[3].Title.Caption := 'Celular';
  grdClient.Columns[4].Title.Caption := 'E-mail';
  grdClient.Columns[4].Width := 200;
  grdClient.Columns[5].Title.Caption := 'Saldo Devedor';
  TNumericField(dmMongo.mqyClient.Fields[5]).DisplayFormat := '#,##0.00';
end;

procedure TfClientControl.sbnWriteOffClick(Sender: TObject);
begin
  try
    dmMongo.FConMongo['zaiko']['client'].Update().Match()
      .Add('cpf', grdClient.DataSource.DataSet.Fields[2].AsString).&End
      .Modify().&Set()
        .Field('forward', GetForward(grdClient.DataSource.DataSet.Fields[2].AsString) - StrToFloat(edtWriteOff.Text))
        .&End
      .&End.Exec;
  except
    on E: Exception do
      MessageDlg('Erro ao atualizar o saldo devedor do cliente: ' + E.Message, mtWarning, [mbOK], 0);
  end;

  try
    dmMongo.FConMongo['zaiko']['sales'].Insert().Values()
      .Add('month', MonthOf(Date))
      .Add('year', YearOf(Date))
      .Add('client', dmMongo.mqyClient.Fields[2].AsString)
      .Add('date_sale', Date)
      .Add('product', 'Pagamento por ' + IfThen(cftType.ItemIndex = 0, 'Cart�o', 'Dinheiro'))
      .Add('subtotal', StrToFloat(edtWriteOff.Text) * -1)
      .Add('way_pay', cftType.ItemIndex + 5).&End.Exec;
  except
    on E: Exception do
    MessageDlg('Erro na venda: ' + E.Message, mtWarning, [mbOK], 0);
  end;
  sbnClosePanel.OnClick(Self);
  sbnSearch.OnClick(Self);
end;

function TfClientControl.GetForward(aCPF: string): Double;
var
  jForward: TJSONValue;
  jRetorno: TJSONObject;
begin
  jRetorno := GetSelect(aCPF, 'client', 'cpf');

  if Assigned(jRetorno) then
  begin
    jForward := jRetorno.Get('forward').JsonValue;
    Result := StrToFloat(jForward.Value);
  end
  else
    Result := 0;
end;

end.
