unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Imaging.pngimage,
  Vcl.Menus, System.ImageList, Vcl.ImgList, Vcl.StdCtrls;

type
  TfMain = class(TForm)
    imgLogo: TImage;
    mnmMain: TMainMenu;
    mniRegistration: TMenuItem;
    mniClient: TMenuItem;
    mniProduct: TMenuItem;
    mniMovement: TMenuItem;
    mniInput: TMenuItem;
    mniOutput: TMenuItem;
    mniFinancial: TMenuItem;
    mniClientControl: TMenuItem;
    mniInventoryControl: TMenuItem;
    mniTotals: TMenuItem;
    mniClose: TMenuItem;
    procedure mniCloseClick(Sender: TObject);
    procedure mniClientClick(Sender: TObject);
    procedure mniProductClick(Sender: TObject);
    procedure mniInputClick(Sender: TObject);
    procedure mniOutputClick(Sender: TObject);
    procedure mniClientControlClick(Sender: TObject);
    procedure mniInventoryControlClick(Sender: TObject);
    procedure mniTotalsClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure OpenForm(FClass: TFormClass; var Instance);
  end;

var
  fMain: TfMain;

implementation

uses
  uClientRegistry, uInput, uSale, uClientControl, uInventoryControl, uTotals,
  uProductRegistry;

{$R *.dfm}

procedure TfMain.mniClientClick(Sender: TObject);
begin
  OpenForm(TfClientRegistry, fClientRegistry);
end;

procedure TfMain.mniClientControlClick(Sender: TObject);
begin
  OpenForm(TfClientControl, fClientControl);
end;

procedure TfMain.mniCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfMain.mniInputClick(Sender: TObject);
begin
  OpenForm(TfInput, fInput);
end;

procedure TfMain.mniInventoryControlClick(Sender: TObject);
begin
  OpenForm(TfInventoryControl, fInventoryControl);
end;

procedure TfMain.mniOutputClick(Sender: TObject);
begin
  OpenForm(TfSale, fSale);
end;

procedure TfMain.mniProductClick(Sender: TObject);
begin
  OpenForm(TfProductRegistry, fProductRegistry);
end;

procedure TfMain.mniTotalsClick(Sender: TObject);
begin
  OpenForm(TfTotals, fTotals);
end;

procedure TfMain.OpenForm(FClass: TFormClass; var Instance);
begin
  if Assigned(TForm(Instance)) then
  begin
    TForm(Instance).SetFocus;
    TForm(Instance).BringToFront;
  end
  else
    Application.CreateForm(FClass, Instance);
end;

end.
