unit uSale;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Buttons,
  ufrmProduct, Vcl.WinXPickers, Vcl.Mask, VCLTee.TeCanvas, Vcl.Grids, Data.DB,
  Vcl.DBGrids, FireDAC.Phys.MongoDBWrapper, udmMongo, System.JSON, System.UITypes,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, System.DateUtils,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uFunctionsDB;

type
  TfSale = class(TForm)
    Label5: TLabel;
    Bevel1: TBevel;
    sbnSave: TSpeedButton;
    sbnCancel: TSpeedButton;
    sbnExit: TSpeedButton;
    Label2: TLabel;
    metCPF: TMaskEdit;
    edtName: TEdit;
    frmProduct1: TfrmProduct;
    Label3: TLabel;
    edtAmount: TEdit;
    Label6: TLabel;
    edtTotalValue: TEdit;
    Label4: TLabel;
    edtDiscount: TEdit;
    Label7: TLabel;
    cftType: TComboFlat;
    grdSalesItems: TDBGrid;
    sbnAddItem: TSpeedButton;
    sbnRemoveItem: TSpeedButton;
    Label8: TLabel;
    edtUnitValue: TEdit;
    lblTotalSale: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sbnExitClick(Sender: TObject);
    procedure metCPFExit(Sender: TObject);
    procedure edtDiscountKeyPress(Sender: TObject; var Key: Char);
    procedure frmProduct1edtCodeExit(Sender: TObject);
    procedure sbnCancelClick(Sender: TObject);
    procedure sbnAddItemClick(Sender: TObject);
    procedure sbnRemoveItemClick(Sender: TObject);
    procedure sbnSaveClick(Sender: TObject);
    procedure edtAmountExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtUnitValueChange(Sender: TObject);
    procedure edtDiscountExit(Sender: TObject);
  private
    gv_totalSale: Double;
    { Private declarations }
    procedure CalculateTotal;
    function GetAmount(aCodeProduct: string): Integer;
    function GetForward(aCPF: string): Double;
  public
    { Public declarations }
  end;

var
  fSale: TfSale;

implementation

{$R *.dfm}

procedure TfSale.CalculateTotal;
var
  lv_amount, lv_unitValue, lv_discount: Double;
begin
  lv_amount := StrToFloatDef(edtAmount.Text, 0);
  lv_unitValue := StrToFloatDef(edtUnitValue.Text, 0);
  lv_discount := StrToFloatDef(edtDiscount.Text, 0);
  edtTotalValue.Text := FloatToStr((lv_amount * lv_unitValue) - lv_discount);
end;

procedure TfSale.edtDiscountExit(Sender: TObject);
begin
  CalculateTotal;
  (Sender as TEdit).Text := FormatFloat('0.00', StrToFloatDef((Sender as TEdit).Text, 0));
end;

procedure TfSale.edtDiscountKeyPress(Sender: TObject; var Key: Char);
begin
  if ((CharInSet(Key, ['0'..'9',',','.']) = False) and (word(Key) <> VK_BACK)) then
    Key := #0;
end;

procedure TfSale.edtUnitValueChange(Sender: TObject);
begin
  (Sender as TEdit).Text := FormatFloat('0.00', StrToFloatDef((Sender as TEdit).Text, 0));
end;

procedure TfSale.edtAmountExit(Sender: TObject);
begin
  CalculateTotal;
end;

procedure TfSale.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  fSale := nil;
end;

procedure TfSale.FormShow(Sender: TObject);
begin
  gv_totalSale := 0;
end;

procedure TfSale.frmProduct1edtCodeExit(Sender: TObject);
begin
  frmProduct1.edtCodeExit(Sender);

  if frmProduct1.edtCode.Text <> '' then
  begin
    edtUnitValue.Text := FloatToStr(frmProduct1.gv_value);
    dmMongo.mtbSalesItems.Active := True;
  end;
end;

procedure TfSale.metCPFExit(Sender: TObject);
var
  v_cr: IMongoCursor;
  jName: TJSONValue;
  jRetorno: TJSONObject;
begin
  v_cr := dmMongo.FConMongo['zaiko']['client'].Find().Match().Add('cpf', metCPF.Text).&End;

  if v_cr.Next then
  begin
    jRetorno := TJSONObject.ParseJSONValue(v_cr.Doc.AsJSON) as TJSONObject;
    jName := jRetorno.Get('full_name').JsonValue;
    edtName.Text := jName.Value;
  end
  else
    metCPF.Text := '   .   .   -  ';
end;

procedure TfSale.sbnAddItemClick(Sender: TObject);
begin
  dmMongo.mtbSalesItems.Append;
  dmMongo.mtbSalesItems.Fields[0].AsInteger := cftType.ItemIndex;
  dmMongo.mtbSalesItems.Fields[1].AsDateTime := Date;
  dmMongo.mtbSalesItems.Fields[2].AsInteger := StrToIntDef(frmProduct1.edtCode.Text, 0);
  dmMongo.mtbSalesItems.Fields[3].AsString := frmProduct1.edtName.Text;
  dmMongo.mtbSalesItems.Fields[4].AsFloat := StrToFloatDef(edtUnitValue.Text, 0);
  dmMongo.mtbSalesItems.Fields[5].AsInteger := StrToIntDef(edtAmount.Text, 0);
  dmMongo.mtbSalesItems.Fields[6].AsFloat := StrToFloatDef(edtDiscount.Text, 0);
  dmMongo.mtbSalesItems.Fields[7].AsFloat := StrToFloatDef(edtTotalValue.Text, 0);
  dmMongo.mtbSalesItems.Fields[8].AsString := cftType.Text;
  dmMongo.mtbSalesItems.Post;
  TNumericField(dmMongo.mtbSalesItems.Fields[4]).DisplayFormat := '#,##0.00';
  TNumericField(dmMongo.mtbSalesItems.Fields[6]).DisplayFormat := '#,##0.00';
  TNumericField(dmMongo.mtbSalesItems.Fields[7]).DisplayFormat := '#,##0.00';

  gv_totalSale := gv_totalSale + StrToFloatDef(edtTotalValue.Text, 0);
  lblTotalSale.Caption := 'Total Venda: ' + FormatFloat('#,##0.00', gv_totalSale);

  frmProduct1.edtCode.Clear;
  frmProduct1.edtName.Clear;
  cftType.ItemIndex := -1;
  cftType.Text := '';
  edtAmount.Clear;
  edtUnitValue.Clear;
  edtDiscount.Clear;
  edtTotalValue.Clear;
  frmProduct1.edtCode.SetFocus;
end;

procedure TfSale.sbnCancelClick(Sender: TObject);
begin
  metCPF.Text := '   .   .   -  ';
  edtName.Clear;
  frmProduct1.edtCode.Clear;
  frmProduct1.edtName.Clear;
  cftType.ItemIndex := -1;
  edtAmount.Clear;
  edtUnitValue.Clear;
  edtDiscount.Clear;
  edtTotalValue.Clear;
  dmMongo.mtbSalesItems.Active := False;
  lblTotalSale.Caption := 'Total Venda: ';
end;

procedure TfSale.sbnExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfSale.sbnRemoveItemClick(Sender: TObject);
begin
  if dmMongo.mtbSalesItems.RecordCount > 0 then
  begin
    gv_totalSale := gv_totalSale - dmMongo.mtbSalesItems.Fields[7].AsFloat;
    lblTotalSale.Caption := 'Total Venda: ' + FormatFloat('#,##0.00', gv_totalSale);
  end;
  dmMongo.mtbSalesItems.Delete;
end;

procedure TfSale.sbnSaveClick(Sender: TObject);
begin
  dmMongo.mtbSalesItems.First;

  while not dmMongo.mtbSalesItems.Eof do
  begin
    try
      dmMongo.FConMongo['zaiko']['product'].Update().Match()
        .Add('code', dmMongo.mtbSalesItems.Fields[2].AsString).&End
        .Modify().&Set()
          .Field('amount', GetAmount(dmMongo.mtbSalesItems.Fields[2].AsString) - dmMongo.mtbSalesItems.Fields[5].AsInteger)
          .&End
        .&End.Exec;
    except
      on E: Exception do
        MessageDlg('Erro ao atualizar o estoque: ' + E.Message, mtWarning, [mbOK], 0);
    end;

    try
      dmMongo.FConMongo['zaiko']['sales'].Insert().Values()
        .Add('month', MonthOf(Date))
        .Add('year', YearOf(Date))
        .Add('client', metCPF.Text)
        .Add('date_sale', dmMongo.mtbSalesItems.Fields[1].AsDateTime)
        .Add('product', dmMongo.mtbSalesItems.Fields[3].AsString)
        .Add('subtotal', StrToFloat(dmMongo.mtbSalesItems.Fields[7].AsString))
        .Add('way_pay', dmMongo.mtbSalesItems.Fields[0].AsInteger).&End.Exec;
    except
      on E: Exception do
      MessageDlg('Erro na venda: ' + E.Message, mtWarning, [mbOK], 0);
    end;

    if dmMongo.mtbSalesItems.Fields[0].AsInteger = 4 then
    begin
      try
        dmMongo.FConMongo['zaiko']['client'].Update().Match()
          .Add('cpf', metCPF.Text).&End
          .Modify().&Set()
            .Field('forward', GetForward(metCPF.Text) + StrToFloat(dmMongo.mtbSalesItems.Fields[7].AsString))
            .&End
          .&End.Exec;
      except
        on E: Exception do
          MessageDlg('Erro ao atualizar o saldo devedor do cliente: ' + E.Message, mtWarning, [mbOK], 0);
      end;
    end;
    dmMongo.mtbSalesItems.Next;
  end;
  sbnCancel.Click;
  MessageDlg('Venda finalizada!', mtInformation, [mbOK], 0);
end;

function TfSale.GetAmount(aCodeProduct: string): Integer;
var
  jAmount: TJSONValue;
  jRetorno: TJSONObject;
begin
  Result := 0;
  jRetorno := GetSelect(aCodeProduct, 'product', 'code');

  if Assigned(jRetorno) then
  begin
    try
      jAmount := jRetorno.Get('amount').JsonValue;

      if jAmount <> nil then
        Result := StrToInt(jAmount.Value);
    except
    end;
  end;
end;

function TfSale.GetForward(aCPF: string): Double;
var
  jForward: TJSONValue;
  jRetorno: TJSONObject;
begin
  Result := 0;
  jRetorno := GetSelect(aCPF, 'client', 'cpf');

  if Assigned(jRetorno) then
  begin
    try
      jForward := jRetorno.GetValue('forward');
      
      if jForward <> nil then
        Result := StrToFloat(jForward.Value);
    except
    end;
  end;
end;

end.
