unit uFunctionsDB;

interface

uses
  FireDAC.Phys.MongoDBWrapper, System.JSON, udmMongo;

function GetSelect(aCode, aCollection, aField: string): TJSONObject;

implementation

function GetSelect(aCode, aCollection, aField: string): TJSONObject;
var
  v_cr: IMongoCursor;
  jRetorno: TJSONObject;
begin
  jRetorno := nil;
  v_cr := dmMongo.FConMongo['zaiko'][aCollection].Find().Match().Add(aField, aCode).&End;

  if v_cr.Next then
  begin
    jRetorno := TJSONObject.ParseJSONValue(v_cr.Doc.AsJSON) as TJSONObject;
  end;
  Result := jRetorno;
end;

end.
