inherited fProductRegistry: TfProductRegistry
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Label5: TLabel
    Left = 203
    Width = 229
    Caption = 'Cadastro de Produto'
    ExplicitLeft = 203
    ExplicitWidth = 229
  end
  inherited sbnSave: TSpeedButton
    OnClick = sbnSaveClick
  end
  inherited sbnCancel: TSpeedButton
    OnClick = sbnCancelClick
  end
  inherited sbnExit: TSpeedButton
    OnClick = sbnExitClick
  end
  inherited sbnDelete: TSpeedButton
    OnClick = sbnDeleteClick
  end
  object Label1: TLabel [6]
    Left = 33
    Top = 63
    Width = 44
    Height = 19
    Caption = 'C'#243'digo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Sylfaen'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel [7]
    Left = 12
    Top = 96
    Width = 65
    Height = 19
    Caption = 'Descri'#231#227'o:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Sylfaen'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel [8]
    Left = 17
    Top = 129
    Width = 60
    Height = 19
    Caption = 'Tamanho:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Sylfaen'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel [9]
    Left = 390
    Top = 129
    Width = 49
    Height = 19
    Caption = 'G'#234'nero:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Sylfaen'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel [10]
    Left = 39
    Top = 162
    Width = 38
    Height = 19
    Caption = 'Valor:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Sylfaen'
    Font.Style = []
    ParentFont = False
  end
  object edtCode: TEdit [11]
    Left = 83
    Top = 60
    Width = 91
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Sylfaen'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object edtName: TEdit [12]
    Left = 83
    Top = 93
    Width = 507
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Sylfaen'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object edtSize: TEdit [13]
    Left = 83
    Top = 126
    Width = 150
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Sylfaen'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object cftGenre: TComboFlat [14]
    Left = 445
    Top = 126
    Width = 145
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Sylfaen'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Items.Strings = (
      'Masculino'
      'Feminino')
  end
  object edtValue: TEdit [15]
    Left = 83
    Top = 159
    Width = 150
    Height = 27
    Hint = 'Valor da Venda'
    Alignment = taRightJustify
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Sylfaen'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
    OnExit = edtValueExit
    OnKeyPress = edtValueKeyPress
  end
  inherited edtId: TEdit
    TabOrder = 5
  end
end
