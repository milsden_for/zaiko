unit uInput;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, Vcl.ExtCtrls, Vcl.StdCtrls, System.UITypes,
  Vcl.WinXPickers, VCLTee.TeCanvas, ufrmProduct, FireDAC.Phys.MongoDBWrapper, System.DateUtils,
  System.JSON;

type
  TfInput = class(TForm)
    sbnSave: TSpeedButton;
    sbnExit: TSpeedButton;
    Label5: TLabel;
    Bevel1: TBevel;
    sbnCancel: TSpeedButton;
    Label1: TLabel;
    edtHistoric: TEdit;
    Label2: TLabel;
    dprSkill: TDatePicker;
    Label3: TLabel;
    edtAmount: TEdit;
    Label4: TLabel;
    Label6: TLabel;
    edtValue: TEdit;
    cftType: TComboFlat;
    frmProduct1: TfrmProduct;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sbnExitClick(Sender: TObject);
    procedure cftTypeClick(Sender: TObject);
    procedure sbnSaveClick(Sender: TObject);
    procedure edtValueExit(Sender: TObject);
    procedure edtValueKeyPress(Sender: TObject; var Key: Char);
    procedure sbnCancelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    function GetAmount(aCodeProduct: string): Integer;
  public
    { Public declarations }
  end;

var
  fInput: TfInput;

implementation

{$R *.dfm}

uses udmMongo, uFunctionsDB;

procedure TfInput.cftTypeClick(Sender: TObject);
begin
  frmProduct1.Enabled := cftType.ItemIndex = 0;
end;

procedure TfInput.edtValueExit(Sender: TObject);
begin
  edtValue.Text := FormatFloat('0.00', StrToFloatDef(edtValue.Text, 0));
end;

procedure TfInput.edtValueKeyPress(Sender: TObject; var Key: Char);
begin
  if ((CharInSet(Key, ['0'..'9',',','.']) = False) and (word(Key) <> VK_BACK)) then
    Key := #0;
end;

procedure TfInput.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  fInput := nil;
end;

procedure TfInput.FormShow(Sender: TObject);
begin
  dprSkill.Date := Date;
end;

function TfInput.GetAmount(aCodeProduct: string): Integer;
var
  jAmount: TJSONValue;
  jRetorno: TJSONObject;
begin
  Result := 0;
  jRetorno := GetSelect(aCodeProduct, 'product', 'code');

  if Assigned(jRetorno) then
  begin
    try
      jAmount := jRetorno.Get('amount').JsonValue;

      if jAmount <> nil then
        Result := StrToInt(jAmount.Value);
    except
    end;
  end;
end;

procedure TfInput.sbnCancelClick(Sender: TObject);
begin
  edtHistoric.Clear;
  dprSkill.Date := Date;
  edtAmount.Clear;
  edtValue.Clear;
  cftType.ItemHeight := -1;
  cftType.Text := '';
  frmProduct1.edtCode.Clear;
  frmProduct1.edtName.Clear;
  frmProduct1.Enabled := False;
  edtHistoric.SetFocus;
end;

procedure TfInput.sbnExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfInput.sbnSaveClick(Sender: TObject);
var
  enabled_frame: Boolean;
begin
  enabled_frame := frmProduct1.Enabled;

  if ( enabled_frame ) and ( frmProduct1.edtCode.Text = '' ) then
    MessageDlg('Necessário escolher um produto!', mtInformation, [mbOK], 0)
  else
  begin
    if ( enabled_frame ) then
    begin
      try
        dmMongo.FConMongo['zaiko']['product'].Update().Match()
          .Add('code', frmProduct1.edtCode.Text).&End
          .Modify().&Set()
            .Field('amount', GetAmount(frmProduct1.edtCode.Text) + StrToInt(edtAmount.Text)).&End
          .&End.Exec;
      except
        on E: Exception do
          MessageDlg('Erro ao atualizar estoque: ' + E.Message, mtWarning, [mbOK], 0);
      end;
    end;

    try
      dmMongo.FConMongo['zaiko']['expenses'].Insert().Values()
        .Add('month', MonthOf(Date))
        .Add('year', YearOf(Date))
        .Add('historic', edtHistoric.Text)
        .Add('type', cftType.ItemIndex)
        .Add('value', StrToFloat(edtValue.Text))
        .Add('skill', dprSkill.Date).&End.Exec;
      sbnCancel.Click;

    if ( enabled_frame ) then
      MessageDlg('Estoque atualizado!', mtInformation, [mbOK], 0)
    else
      MessageDlg('Salvo com sucesso!', mtInformation, [mbOK], 0);
    except
      on E: Exception do
        MessageDlg('Erro ao salvar: ' + E.Message, mtWarning, [mbOK], 0);
    end;
  end;
end;

end.
