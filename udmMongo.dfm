object dmMongo: TdmMongo
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 282
  Width = 627
  object FDConnection1: TFDConnection
    Params.Strings = (
      'DriverID=Mongo')
    Connected = True
    LoginPrompt = False
    Left = 24
    Top = 8
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 40
    Top = 64
  end
  object FDPhysMongoDriverLink1: TFDPhysMongoDriverLink
    Left = 50
    Top = 120
  end
  object mtbSalesItems: TFDMemTable
    FieldDefs = <
      item
        Name = 'mtbSalesItemsFieldCodType'
        DataType = ftInteger
      end
      item
        Name = 'mtbSalesItemsFieldDtSale'
        DataType = ftDate
      end
      item
        Name = 'mtbSalesItemsFieldCode'
        DataType = ftInteger
      end
      item
        Name = 'mtbSalesItemsFieldDesc'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'mtbSalesItemsFieldUnitValue'
        DataType = ftFloat
      end
      item
        Name = 'mtbSalesItemsFieldAmount'
        DataType = ftInteger
      end
      item
        Name = 'mtbSalesItemsFieldDiscount'
        DataType = ftFloat
      end
      item
        Name = 'mtbSalesItemsFieldTotal'
        DataType = ftFloat
      end
      item
        Name = 'mtbSalesItemsFieldType'
        DataType = ftString
        Size = 50
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvPersistent, rvSilentMode]
    ResourceOptions.Persistent = True
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 241
    Top = 13
  end
  object dseSalesItems: TDataSource
    DataSet = mtbSalesItems
    Left = 241
    Top = 69
  end
  object mqyProduct: TFDMongoQuery
    FormatOptions.AssignedValues = [fvFmtDisplayNumeric]
    Connection = FDConnection1
    Left = 400
    Top = 16
  end
  object dseProduct: TDataSource
    DataSet = mqyProduct
    Left = 400
    Top = 72
  end
  object mqyClient: TFDMongoQuery
    Connection = FDConnection1
    Left = 536
    Top = 16
  end
  object dseClient: TDataSource
    DataSet = mqyClient
    Left = 536
    Top = 72
  end
  object dseEspenses: TDataSource
    DataSet = mqyExpenses
    Left = 392
    Top = 208
  end
  object dseTotalDaily: TDataSource
    DataSet = mtbTotalDaily
    Left = 528
    Top = 208
  end
  object mtbTotalDaily: TFDMemTable
    FieldDefs = <
      item
        Name = 'mtbTotalDailyFieldSaleDate'
        DataType = ftDate
      end
      item
        Name = 'mtbTotalDailyFieldSubtotal'
        DataType = ftFloat
      end
      item
        Name = 'mtbTotalDailyFieldWayPay'
        DataType = ftString
        Size = 20
      end>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 528
    Top = 152
    object mtbTotalDailymtbTotalDailyFieldSaleDate: TDateField
      FieldName = 'mtbTotalDailyFieldSaleDate'
    end
    object mtbTotalDailymtbTotalDailyFieldSubtotal: TFloatField
      FieldName = 'mtbTotalDailyFieldSubtotal'
      DisplayFormat = '#,##0.00'
    end
    object mtbTotalDailymtbTotalDailyFieldWayPay: TStringField
      FieldName = 'mtbTotalDailyFieldWayPay'
    end
  end
  object mqyExpenses: TFDMongoQuery
    UpdateOptions.KeyFields = '_id'
    Connection = FDConnection1
    Left = 392
    Top = 160
  end
  object mqyHistoricClient: TFDMongoQuery
    UpdateOptions.KeyFields = '_id'
    Connection = FDConnection1
    Left = 264
    Top = 160
  end
  object dseHistoricClient: TDataSource
    DataSet = mqyHistoricClient
    Left = 264
    Top = 208
  end
end
