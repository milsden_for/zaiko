unit uTotals;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.WinXPickers, Vcl.ExtCtrls, ADODB,
  Vcl.StdCtrls, Vcl.Buttons, Data.DB, Vcl.Grids, Vcl.DBGrids, Vcl.ComCtrls,
  udmMongo, System.JSON, DateUtils, FireDAC.Phys.MongoDBWrapper, REST.Response.Adapter;

type
  TfTotals = class(TForm)
    Label1: TLabel;
    dprPeriod: TDatePicker;
    Label2: TLabel;
    edtCash: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    edtCredit: TEdit;
    edtDebt: TEdit;
    edtCheck: TEdit;
    edtForward: TEdit;
    edtPayC: TEdit;
    edtPayD: TEdit;
    sbnExit: TSpeedButton;
    pnlButtons: TPanel;
    pclSubtotals: TPageControl;
    pnlTitle: TPanel;
    tstSubTotals: TTabSheet;
    tstExpenses: TTabSheet;
    tstTodalDaily: TTabSheet;
    grdTotalDaily: TDBGrid;
    pnlSubtotals: TPanel;
    grdExpenses: TDBGrid;
    sttExpensiveTotal: TStaticText;
    sttTotalDaily: TStaticText;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure sbnExitClick(Sender: TObject);
    procedure dprPeriodChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtCashChange(Sender: TObject);
    procedure pclSubtotalsChange(Sender: TObject);
    procedure grdTotalDailyCellClick(Column: TColumn);
  private
    { Private declarations }
    procedure CalculateSubtotals(aMonth, aYear: Word);
    procedure ListExpenses(aMonth, aYear: Word);
    procedure ListTotalDaily(aMonth, aYear: Word);
  public
    { Public declarations }
    procedure JsonToDataset(aDataset: TDataSet; aJSON :string);
  end;

var
  fTotals: TfTotals;

implementation

{$R *.dfm}

procedure TfTotals.CalculateSubtotals(aMonth, aYear: Word);
var
  v_cr: IMongoCursor;
  jReturn: TJSONObject;
  jId, jSubtotal: TJSONValue;
begin
  edtCash.Clear;
  edtCredit.Clear;
  edtDebt.Clear;
  edtCheck.Clear;
  edtForward.Clear;
  edtPayC.Clear;
  edtPayD.Clear;

  v_cr := dmMongo.FConMongo['zaiko']['sales'].Aggregate()
    .Match()
      .Add('month', aMonth)
      .Add('year', aYear)
    .&End
    .Group()
      .Add('_id', '$way_pay')
      .BeginObject('sumSubtotal')
        .Add('$sum', '$subtotal')
      .EndObject
    .&End;

  while v_cr.Next do
  begin
    jReturn := TJSONObject.ParseJSONValue(v_cr.Doc.AsJSON) as TJSONObject;
    jId := jReturn.GetValue('_id');
    jSubtotal := jReturn.GetValue('sumSubtotal');

    case StrToInt(jId.Value) of
      0: edtCash.Text := jSubtotal.Value;
      1: edtCredit.Text := jSubtotal.Value;
      2: edtDebt.Text := jSubtotal.Value;
      3: edtCheck.Text := jSubtotal.Value;
      4: edtForward.Text := jSubtotal.Value;
      5: edtPayC.Text := Copy(jSubtotal.Value, 2, Length(jSubtotal.Value));
      6: edtPayD.Text := Copy(jSubtotal.Value, 2, Length(jSubtotal.Value));
    end;
  end;
end;

procedure TfTotals.dprPeriodChange(Sender: TObject);
var
  lv_month, lv_year: Word;
begin
  dmMongo.mtbTotalDaily.Active := False;
  dmMongo.mtbTotalDaily.Active := True;
  lv_month := MonthOf(dprPeriod.Date);
  lv_year := YearOf(dprPeriod.Date);
  CalculateSubtotals(lv_month, lv_year);
  ListExpenses(lv_month, lv_year);
  ListTotalDaily(lv_month, lv_year);
end;

procedure TfTotals.edtCashChange(Sender: TObject);
begin
  try
    (Sender as TEdit).Text := FormatFloat('#,##0.00', StrToFloat((Sender as TEdit).Text));
  except
  end;
end;

procedure TfTotals.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  fTotals := nil;
end;

procedure TfTotals.FormShow(Sender: TObject);
begin
  dprPeriod.Date := Date;
  pclSubtotals.ActivePageIndex := 0;
end;

procedure TfTotals.grdTotalDailyCellClick(Column: TColumn);
var
  vl_i: Integer;
  vl_sum: Double;
begin
  vl_sum := 0;

  if grdTotalDaily.SelectedRows.Count > 1 then
  begin
    for vl_i := 0 to grdTotalDaily.SelectedRows.Count -1 do
    begin
      grdTotalDaily.DataSource.DataSet.GotoBookmark(grdTotalDaily.SelectedRows.Items[vl_i]);
      vl_sum := vl_sum + dmMongo.mtbTotalDaily.Fields[1].AsFloat;
    end;
    sttTotalDaily.Caption := 'Total Di�rio: ' + FormatFloat('#,##0.00', vl_sum);
  end
  else
    sttTotalDaily.Caption := 'Total Di�rio: ';
end;

procedure TfTotals.JsonToDataset(aDataset: TDataSet; aJSON: string);
var
  JObj: TJSONArray;
  vConv : TCustomJSONDataSetAdapter;
begin
  if (aJSON = EmptyStr) then
  begin
    Exit;
  end;

  JObj := TJSONObject.ParseJSONValue(aJSON) as TJSONArray;
  vConv := TCustomJSONDataSetAdapter.Create(Nil);

  try
    vConv.Dataset := aDataset;
    vConv.UpdateDataSet(JObj);
  finally
    vConv.Free;
    JObj.Free;
  end;
end;

procedure TfTotals.ListExpenses(aMonth, aYear: Word);
var
  vl_sum: Double;
begin
  vl_sum := 0;
  dmMongo.mqyExpenses.Close;
  dmMongo.mqyExpenses.QMatch := '{month: ' + IntToStr(aMonth) + ', year: ' + IntToStr(aYear) + '}';
  dmMongo.mqyExpenses.QSort := '{skill: 1}';
  dmMongo.mqyExpenses.Open;
  TNumericField(dmMongo.mqyExpenses.Fields[5]).DisplayFormat := '#,##0.00';
  dmMongo.mqyExpenses.First;

  while not dmMongo.mqyExpenses.Eof do
  begin
    vl_sum := vl_sum + dmMongo.mqyExpenses.Fields[5].AsFloat;
    dmMongo.mqyExpenses.Next;
  end;
  sttExpensiveTotal.Caption := 'Total de Gastos: ' + FormatFloat('#,##0.00', vl_sum);
end;

procedure TfTotals.ListTotalDaily(aMonth, aYear: Word);
var
  v_cr: IMongoCursor;
  lv_way_pay: Integer;
  lv_sale_date: TDate;
  lv_subtotal: Double;
  sWayPay, sSaleDate, sSubtotal: string;
begin
  v_cr := dmMongo.FConMongo['zaiko']['sales'].Aggregate()
    .Match()
      .Add('month', aMonth)
      .Add('year', aYear)
    .&End
    .Group()
      .BeginObject('_id')
        .Add('way_pay', '$way_pay')
        .Add('sale', '$date_sale')
      .EndObject
      .BeginObject('sum_subtotal')
        .Add('$sum', '$subtotal')
      .EndObject
    .&End;

  while v_cr.Next do
  begin
    sSaleDate := Copy(v_cr.Doc.AsJSON, 38, 10);
    sSubtotal := Copy(v_cr.Doc.AsJSON, 86, Pos('}', v_cr.Doc.AsJSON) - 1);
    lv_way_pay := StrToInt(Copy(v_cr.Doc.AsJSON, 19, 1));
    lv_sale_date := StrToDate(FormatDateTime('dd/MM/yyyy', StrToDate(Copy(sSaleDate,9,2) + '/' + Copy(sSaleDate,6,2) + '/' + Copy(sSaleDate,1,4))));
    lv_subtotal := StrToFloat(Copy(sSubtotal, 1, Length(sSubtotal) -1));

    case lv_way_pay of
      0: sWayPay := 'Dinheiro';
      1: sWayPay := 'Cr�dito';
      2: sWayPay := 'D�bito';
      3: sWayPay := 'Cheque';
      4: sWayPay := 'A prazo';
      5: sWayPay := 'Pagamento C';
      6: sWayPay := 'Pagamento D';
    end;

    dmMongo.mtbTotalDaily.Append;
    dmMongo.mtbTotalDaily.Fields[0].AsDateTime := lv_sale_date;
    dmMongo.mtbTotalDaily.Fields[1].AsFloat := Abs(lv_subtotal);
    dmMongo.mtbTotalDaily.Fields[2].AsString := sWayPay;
    dmMongo.mtbTotalDaily.Post;
  end;
  dmMongo.mtbTotalDaily.IndexFieldNames := grdTotalDaily.Columns[0].FieldName;
end;

procedure TfTotals.pclSubtotalsChange(Sender: TObject);
begin
  sttExpensiveTotal.Visible := pclSubtotals.ActivePageIndex = 1;
  sttTotalDaily.Visible := pclSubtotals.ActivePageIndex = 2;
end;

procedure TfTotals.sbnExitClick(Sender: TObject);
begin
  dmMongo.mtbTotalDaily.Active := False;
  Close;
end;

end.
